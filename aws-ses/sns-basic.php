<?php

// Fetch the raw POST body containing the message
$postBody = file_get_contents('php://input');

$sns = json_decode($postBody);

function check_property_or_exit($object, $property) {
    if (!property_exists($object, $property)) {
        error_log('aws-sns-script: Attribute '. $property . ' not found');
        exit();
    }
}

// JSON malformed
if ($sns === null) {
    error_log('aws-sns-script: Request content could not be decoded as JSON');
    exit();
}

check_property_or_exit($sns, 'Type');

if ('SubscriptionConfirmation' == $sns->Type) {

    check_property_or_exit($sns, 'SubscribeURL');

    // Confirm AWS SNS Topic subscription
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $sns->SubscribeURL);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 5);
    $curl_output = curl_exec($ch);
    curl_close($ch);

}
else if ('Notification' == $sns->Type) {

    check_property_or_exit($sns, 'Message');

    $message = json_decode($sns->Message);

    // Message malformed
    if ($message === null) {
        error_log('aws-sns-script: Message attribute could not be parsed');
        exit();
    }

    check_property_or_exit($message, 'bounce');
    check_property_or_exit($message->bounce, 'bouncedRecipients');

    foreach ($message->bounce->bouncedRecipients as $bounce) {
        $email = $bounce->emailAddress;

        if ('Permanent' == $message->bounce->bounceType) {
            // Add your application code here
            // All mails to this email address will fail
        }
        else if ('Transient' == $message->bounce->bounceType) {
            // Add your application code here
            // Mails to this email address will probably fail during some time.
        }
        else {
            // Add your application code here
            // The reason of the bounce could not be determined by AWS SES
        }
    }

}
