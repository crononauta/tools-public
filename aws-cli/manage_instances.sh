#!/bin/bash
#

set -u

# Load Settings
source "manage_instances.conf"

# Private variables
AWS=$(which aws)
GREP=$(which grep)
AWK=$(which awk)
SED=$(which sed)
CUT=$(which cut)
HEAD=$(which head)
CAT=$(which cat)
RM=$(which rm)
TRAP=$(which trap)
TOUCH=$(which touch)
SORT=$(which sort)
OPERATION=""
INSTANCE=""
REGION=""
SHOWN_PROD=

# Function declarations
function usage() {
    echo
    echo "Usage: ${0} -o [list|start|stop] [-i instance-id] [-r region]"
    echo
    echo "You may need to install and/or configure aws-cli by typing 'aws configure' if you have not done so"
    echo

    exit 1
}

function describe_instance() {
    instance=$1

    OUTPUT_FILE="/tmp/$0.$$.${instance}"
    $AWS ec2 --region $region --output text describe-instances --filters "Name=instance-id,Values=${instance}" --profile "$ACCOUNT" > "$OUTPUT_FILE"
    STATE=$($CAT "$OUTPUT_FILE" | $GREP "STATE	" | $AWK '{print $3; }')

    TEST=$($CAT "$OUTPUT_FILE" | $GREP INSTANCES | $AWK '{ print $12; }' | $CUT -c 1-16 | $SED -e 's/-/\//g' -e 's/T/ /')

    NAME=$($CAT "$OUTPUT_FILE"| $GREP "TAGS" | $GREP -v "aws:autoscaling:groupName" | $GREP "Name" | $AWK '{ print $3; }')
    REGION=$($CAT "$OUTPUT_FILE" | $GREP "PLACEMENT" | $AWK '{print $2; }')
    ASSOCIATION=$($CAT "$OUTPUT_FILE" | $GREP "ASSOCIATION" | $HEAD -n 1 | $AWK '{print $2; }')

    if [[ "$TEST" == ip* ]]; then
        LAUNCH_TIME=$($CAT "$OUTPUT_FILE" | $GREP INSTANCES | $AWK '{ print $11; }' | $CUT -c 1-16 | $SED -e 's/-/\//g' -e 's/T/ /')
        TYPE=$($CAT "$OUTPUT_FILE" | $GREP INSTANCES | $AWK '{ print $9; }')
        PRIVATE_IP=$($CAT "$OUTPUT_FILE" | $GREP INSTANCES | $AWK '{ print $13; }')
    else
        LAUNCH_TIME=$($CAT "$OUTPUT_FILE" | $GREP INSTANCES | $AWK '{ print $12; }' | $CUT -c 1-16 | $SED -e 's/-/\//g' -e 's/T/ /')
        TYPE=$($CAT "$OUTPUT_FILE" | $GREP INSTANCES | $AWK '{ print $10; }')
        PRIVATE_IP=$($CAT "$OUTPUT_FILE" | $GREP INSTANCES | $AWK '{ print $14; }')
    fi

    PROD_NAME="${NAME/-staging/}"
    PROD_NAME="${PROD_NAME/-detached/}"

    EXTRA=""

    if [[ "${STATE}" == "running" || "${ASSOCIATION}" != "" ]]
    then
        if [[ "$TEST" == ip* ]]; then
            PUBLIC_IP=$($CAT "$OUTPUT_FILE" | $GREP INSTANCES | $AWK '{ print $15; }')
        else
            PUBLIC_IP=$($CAT "$OUTPUT_FILE" | $GREP INSTANCES | $AWK '{ print $16; }')
        fi

    else
        PUBLIC_IP="    -    "
    fi

    if [[ "${ASSOCIATION}" != "amazon" && "${ASSOCIATION}" != "" ]]
    then
        EXTRA="[EIP]"
    fi

    echo -e " » ${STATE}\t[${LAUNCH_TIME}]\t${instance}\t(${REGION}) | ${TYPE}\t${PRIVATE_IP}\t${PUBLIC_IP}\t « ${NAME}\t${EXTRA}" >> /tmp/$0.$$.output

    if [[ ! $SHOWN_PROD && " ${SHOWN_PROD[@]} " =~ " ${PROD_NAME} " ]]; then
        return 0
    fi

    SHOWN_PROD+=("$PROD_NAME")

    PROD_INSTANCES=$($AWS ec2 --region $region --output text describe-instances --filters "Name=tag:Name,Values=$PROD_NAME" --profile "$ACCOUNT" | $GREP INSTANCES | $AWK '{ print $8 ":" $9;}' | wc -l )

    if [[ "$PROD_INSTANCES" != "0" ]]; then
        echo -e " » Total $PROD_INSTANCES instances of $PROD_NAME" >> /tmp/$0.$$.output
    fi

    PROD_INSTANCES=$($AWS ec2 --region $region --output text describe-instances --filters "Name=tag:Name,Values=$PROD_NAME-detached" --profile "$ACCOUNT" | $GREP INSTANCES | $AWK '{ print $8 ":" $9;}' | wc -l )

    if [[ "$PROD_INSTANCES" != "0" ]]; then
        echo -e " » Total $PROD_INSTANCES instances of $PROD_NAME-detached" >> /tmp/$0.$$.output
    fi

    PROD_INSTANCES=$($AWS ec2 --region $region --output text describe-instances --filters "Name=tag:Name,Values=$PROD_NAME-staging" --profile "$ACCOUNT" | $GREP INSTANCES | $AWK '{ print $8 ":" $9;}' | wc -l )

    if [[ "$PROD_INSTANCES" != "0" ]]; then
        echo -e " » Total $PROD_INSTANCES instances of $PROD_NAME-staging" >> /tmp/$0.$$.output
    fi

}

function get_instances() {

    $TOUCH /tmp/$0.$$.output

    if [[ -z "$AWS_REGIONS" ]]
    then
        REGIONS=$($AWS ec2 describe-regions | $AWK '{ print $3; }')
    else
        REGIONS=$AWS_REGIONS
    fi

    for region in $REGIONS
    do
        TEAM="*$TEAM*"
        INSTANCES_RESULT=$($AWS ec2 --region $region --output text describe-instances --filters "Name=tag:team,Values=$TEAM" --profile "$ACCOUNT" )

        INSTANCES=$(echo "$INSTANCES_RESULT" | $GREP INSTANCES | $AWK '{ print $8 ":" $9;}')
        for candidates in $INSTANCES
        do
            instance=$(echo "$candidates" | grep -oP "(:(i-[0-9a-z]+)|(^i-[0-9a-z]+))" | sed "s/://")
            describe_instance "${instance}"
        done

        echo "$INSTANCES_RESULT" | grep "Name" | grep -v "aws:autoscaling:groupName" | awk '{print $3}' | sort | uniq -c | awk '{print " » Total " $1 " instances of " $2}' >> /tmp/$0.$$.output

    done
}

function start() {
    $AWS ec2 start-instances --instance-ids $1 --region $2 --profile "$ACCOUNT"
}

function stop() {
    $AWS ec2 stop-instances --instance-ids $1 --region $2 --profile "$ACCOUNT"
}

function finish() {
    $RM -f /tmp/$0.$$.*
}

# trap commands
$TRAP finish EXIT SIGHUP SIGINT SIGQUIT SIGKILL SIGTERM

# Gets options
while getopts ":o::i::r:" arg; do
    case "$arg" in
        o)
            OPERATION="${OPTARG}"
            ;;
        i)
            INSTANCE=${OPTARG}
            ;;
        r)
            REGION=${OPTARG}
            ;;
        *)
            usage
            ;;
    esac
done

# Main flow

if [[ -z "${AWS}" ]]
then
    usage
fi

if [[ -z "${OPERATION}" || ! -z "${OPERATION}" && "${OPERATION}" != "list" && -z "${INSTANCE}" ]]
then
    usage
fi

case "${OPERATION}" in
    list)
        echo -e "Retrieving data...\n"
        get_instances
        $CAT /tmp/$0.$$.output | $SORT | uniq
        finish
        ;;
    start)
        start ${INSTANCE} ${REGION}
        finish
        ;;
    stop)
        stop ${INSTANCE} ${REGION}
        finish
        ;;
esac
