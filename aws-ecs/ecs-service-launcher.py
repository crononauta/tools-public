#!/usr/bin/env python3

import boto3
import argparse
import sys
import os

ACCESS_KEY = os.environ.get('ACCESS_KEY')
SECRET_KEY = os.environ.get('SECRET_KEY')

session = boto3.Session(
    aws_access_key_id=ACCESS_KEY,
    aws_secret_access_key=SECRET_KEY,
)
client = session.client('ecs')
paginator = client.get_paginator('list_services')

parser = argparse.ArgumentParser(description='Launch your ECS service',
                                 epilog='Have a nice coding session!')
parser.add_argument('-p', '--project', type=str)
parser.add_argument('-e', '--env', type=str,
                    choices=['dev', 'staging', 'pre', 'qa'])
parser.add_argument('-c', '--country', type=str)
args = parser.parse_args()


if len(sys.argv) < 2:
    parser.print_help()
    sys.exit()

if args.project is None:
    print('Error: You must specify a project')
    sys.exit()

if args.env is None:
    print('Error: You must specify an environment to work with')
    sys.exit()


def parse_arn(arn):
    elements = arn.split(':')
    result = {
        'partition': elements[1],
        'service': elements[2],
        'region': elements[3],
        'account-id': elements[4],
        'resource-id': elements[5],
    }
    return result


def main():
    project = args.project
    country = args.country
    environment = args.env

    clusters = {
        'dev': 'webapps-dev',
        'prod': 'webapps',
    }
    cluster = clusters.get(environment, 'webapps-staging')

    search = '-'.join([project, environment, country])

    response_iterator = paginator.paginate(
        cluster=cluster,
        PaginationConfig={
            'PageSize': 100
        }
    )

    for each_page in response_iterator:
        for arn in each_page['serviceArns']:
            if search in arn:
                resourceid = parse_arn(arn)['resource-id']
                service = resourceid.split('/')[2]
                break

    if 'cron' in service:
        desired_count = 1
    else:
        desired_count = 2

    print('Your about to launch the ECS ' + service + ' service.')
    answer = input(' > Are you sure you want to continue? [y/N] ')

    if answer == 'y':
        r = client.update_service(cluster=cluster, service=service,
                                  desiredCount=desired_count)
        print('\nThe ' + service + '\'s containers are being launched.')
        print('They will be available in a few minutes')

    else:
        sys.exit()


if __name__ == "__main__":
    main()
