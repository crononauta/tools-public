# Introduction

This script is meant to launch ECS services containers.


## Getting started

Assuming that you have a supported version of Python installed, you can first
install boto3 from PyPi with:

    $ pip3 install boto3

or

    $ python3 -m pip install boto3


After installing boto3, next, export your credentials as environment variables:

    $ export ACCESS_KEY='your-access-key-here'
    $ export SECRET_KEY='your-secret-key-here'


or set up your credentials in e.g. ~/.aws/credentials:

    [default]
    aws_access_key_id = YOUR_KEY
    aws_secret_access_key = YOUR_SECRET

Then, set up a default region (in e.g. ~/.aws/config):
    
    [default]
    region=eu-west-1


## Using the ECS launcher

    $ ./ecs-service-launcher.py --project your-project --country country-code --env dev

Getting help:

    $ ./ecs-service-launcher.py --help

