#!/bin/bash
set -ef -o pipefail

#
# Copyright (c) 2017, crononauta.com | Available under public domain
#
# Converts a bunch of elements into a JSON.
# Usage:
#
#   echo key value | json.sh
#   {"key": "value"}
#
#   echo crononauta 24x7-sysops | json.sh
#   {"crononauta": "24x7-sysops"}
#

arr=( );

while read key value; do 
    arr=("${arr[@]}" $key $value)
done

items=(${arr[@]})
len=${#arr[@]}

echo -n "{"
for (( i=0; i<len; i+=2 )); do
    echo -n "\"${items[i]}\": \"${items[i+1]}\""
    if [[ "$i" -lt "$((len-2))" ]] ; then
        echo -n ", "
    fi
done
echo "}"
